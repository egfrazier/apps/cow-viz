import React from "react";
import BarChart from './viz_modules/BarChart';

import { 
    gql,
    useQuery
} from "@apollo/client";

const ALL_SYSTEM_MEMBERSHIPS = gql`
  query {
    allSystemMembers {
      edges {
        node {
          id
          year
          abbr
          ccode
        }
      }
    }
  }
`;

function System() {
    const { loading, error, data } = useQuery(ALL_SYSTEM_MEMBERSHIPS);
    
    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error!</p>;
    
    var systemMembers = data['allSystemMembers']['edges'];


    // Generate a map of year to total number of states.
    var stateYear = [];

    for (let i = 0; i < systemMembers.length; i++) {
        var currentYear = systemMembers[i]['node']['year']
        if (!stateYear.map(a => a.year).includes(currentYear)) {
            stateYear.push({
                'year': currentYear,
                'total': 1
            })
            continue
        }

        var stateYearIndex = stateYear.map(a => a.year).indexOf(currentYear);
        stateYear[stateYearIndex]['total']++
    }

    var payload = []

    for (let i =0; i < stateYear.length; i++) {
        if (i % 10 === 0) {
            payload.push(stateYear[i])
        }
    }


    return (
        <div>
            <h1>International State System Membership (1816-2016)</h1>
            <p>The Correlates of War data sets track the evolution of the international state system from 1816 (just after end of the Napoleonic Wars and the subsequent Congress of Vienna to 2016. The following chart shows the total number of states in the system over time.</p>
            <BarChart data={ payload } />
        </div>
    );
}

export default System;