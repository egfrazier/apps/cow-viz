import React, { Component } from "react";

import { 
    gql,
    useQuery
} from "@apollo/client";

const ALL_STATES = gql`
  query {
    allStates {
      edges {
        node {
          id
          ccode
          abbr
          name
        }
      }
    }
  }
`;

const SINGLE_STATE = gql`
  query State($ccode: Int!) {
    state(ccode: $ccode)
  }
`;

function StateSelect() {
    const { loading, error, data } = useQuery(ALL_STATES);

    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error!</p>;


    var states = data['allStates']['edges'];
    return (
      <div>
          <select name="stateSelect" id="stateSelectFrom">
            {states.map(function(state, index){
              return (
                <option value={state['node']['ccode']} key={index}>{state['node']['name']}</option>
              );
            })}
          </select>
      </div>
    );
}

function StateDetail(ccode) {
  console.log(ccode);
  const { loading, error, data } = useQuery(SINGLE_STATE, {
    variables: ccode,
  });
  console.log(data);

  if (loading) return <p>Loading...</p>;
  if (error && data == null) {
    return <div></div>;
  } else if (error) {
    return <p>Error</p>;
  } 

  if ({ ccode }) {
    console.log(data['state']);
    var stateJsonString = data['state'].replace(/'/g, "\"");
    console.log(stateJsonString);
    var currentState = JSON.parse(stateJsonString);
    return (
      <div>
        <table>
            <tbody>
              <tr>
                <td>Country Code</td>
                <td>{currentState.ccode}</td>
              </tr>
              <tr>
                <td>Name</td>
                <td>{currentState.name}</td>
              </tr>
              <tr>
                <td>Abbreviation</td>
                <td>{currentState.abbr}</td>
              </tr>
            </tbody>
        </table>
      </div>
    );
  } else {
    return (<div></div>);
  }
}
 
class StateForm extends Component {
  constructor(props) {
    super(props);
    this.state = {stateData: null}

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    this.setState({stateData: event.target.elements.stateSelect.value})
    event.preventDefault();
  }

  render () {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <label>Select a state:</label>
          <StateSelect />
          <input type="submit" value="Submit" />
        </form>
        <StateDetail ccode={this.state.stateData} />
      </div>
    );
  }
}
export default StateForm;