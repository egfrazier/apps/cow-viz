import React, { Component } from "react";

class About extends Component {
    render() {
        return (
            <div>
                <h2>About</h2>
                <p>This site contains visualizations of the Correlates of War Project. The project was originally founded by J. David Singer of University Michigan. In 2001, the project control was transferred to Penn State.</p>

                <p>The data itself exists in a collection of spreadsheets. This site is built on an UNOFFICAL GraphQL API specifically created for this project.</p>

                <p>This project is Open Source. See the <a href="/contact">Contact</a> page for links to the source code.</p>
            </div>
        );
    }
}

export default About;