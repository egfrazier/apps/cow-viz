import React, { Component } from "react";
import {
    Route,
    Routes,
    NavLink,
    BrowserRouter
} from "react-router-dom";
import About from "./About";
import StateForm from "./States";
import System from "./System";
import Contact from "./Contact";

class Main extends Component {
    render() {
        return (
            <BrowserRouter>
                <div>
                    <h1>Correlates of War Data Visualization</h1>
                    <ul className="header">
                        <li><NavLink to="/">About</NavLink></li>
                        <li><NavLink to="/states">States</NavLink></li>
                        <li><NavLink to="/system">System</NavLink></li>
                        <li><NavLink to="/contact">Contact</NavLink></li>
                    </ul>
                    <div className="content">
                        <Routes>
                            <Route path="/" element={<About />} />
                            <Route path="/states" element={<StateForm />} />
                            <Route path="/system" element={<System />} />
                            <Route path="/contact" element={<Contact />} />
                        </Routes>
                    </div>
                </div>
            </BrowserRouter>
        );
    }
}

export default Main;