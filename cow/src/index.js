import React from "react";
import ReactDOM from "react-dom";
import Main from "./Main";
import "./index.css";

import {
    ApolloClient,
    InMemoryCache,
    ApolloProvider
} from "@apollo/client";

const client = new ApolloClient({
  uri: 'http://api.cow.egfrazier.com' + '/graphql',
  cache: new InMemoryCache(),
});

ReactDOM.render(
    <ApolloProvider client={client}>
      <Main/>
    </ApolloProvider>,
    document.getElementById("root")
);
