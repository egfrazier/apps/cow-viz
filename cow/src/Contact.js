import React, { Component } from "react";
 
class Contact extends Component {
  render() {
    return (
      <div>
        Created by Elizabeth Frazier<br />
        <a href="https://gitlab.com/egfrazier/apps/cow-api">API Source Code</a><br />
        <a href="https://gitlab.com/egfrazier/apps/cow-viz">Frontend Source Code</a>
      </div>
    );
  }
}
 
export default Contact;